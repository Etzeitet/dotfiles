export PATH=$HOME/bin:/usr/local/bin:$HOME/.local/bin:$PATH

# Path to your oh-my-zsh installation.
if [[ -d "/usr/share/oh-my-zsh" ]]; then
    export ZSH="/usr/share/oh-my-zsh"
else
    export ZSH="$HOME/.oh-my-zsh"
fi

# Theme settings
ZSH_THEME="spaceship"

SPACESHIP_TIME_SHOW="true"
SPACESHIP_PROMPT_ORDER=(time 
user 
host 
dir 
git 
venv 
pyenv 
exec_time 
line_sep 
vi_mode 
jobs 
char)

SPACESHIP_USER_SHOW="always"
SPACESHIP_USER_COLOR="green"
SPACESHIP_USER_SUFFIX=""
SPACESHIP_USER_PREFIX=""

SPACESHIP_HOST_SHOW="always"
SPACESHIP_HOST_PREFIX="@"

SPACESHIP_PYENV_SHOW="always"

# oh-my-zsh settings
DISABLE_UNTRACKED_FILES_DIRTY="true"
HIST_STAMPS="yyyy-mm-dd"

# Plugins
plugins=(git copyfile dnf extract pyenv python sudo systemd)

# External configs
source $ZSH/oh-my-zsh.sh


# Exports

# Pipenv + Pyenv
export PATH="$PATH:$HOME/.local/bin:$HOME/.pyenv"
export PYENV_ROOT="$HOME/.pyenv"

export TERM=xterm-256color
export EDITOR=vim

# pyenv init messes with PATH in order to function. Do not make changes to PATH
# below this comment. You may unleash tentacled wossnames from the Dungeon Dimensions!

# Initialise pyenv - this messes with PATH, so heed the warning above!
if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
fi

# Functions

# mkdir + cd
function mcd() {
    mkdir -p "${1}" && cd "${1}";
}

# twilix tweak
if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
    source /etc/profile.d/vte*.sh
fi

# Aliases
alias sudo='sudo '
alias zshconfig="vim ~/.zshrc"
alias zshreload="source ~/.zshrc"
alias jfdi='sudo $(fc -ln -1)'
alias dotconf='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
