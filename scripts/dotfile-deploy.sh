#!/usr/bin/env bash
#
# This script based on Bash Boilerplate: https://github.com/alphabetum/bash-boilerplate
#

# Treat unset variables and parameters other than the special parameters '@' or
# '*' as an error when performing parameter expansion. An 'unbound variable'
# error message will be written to the standard error, and a non-interactive
# shell will exit
set -o nounset

# Exit immediately if a pipeline returns non-zero
set -o errexit

# Print a helpful message if a pipeline with non-zero exit code causes the
# script to exit as described above.
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR

# Allow the above trap be inherited by all functions in the script.
#
# Short form: set -E
set -o errtrace

# Return value of a pipeline is the value of the last (rightmost) command to
# exit with a non-zero status, or zero if all commands in the pipeline exit
# successfully.
set -o pipefail

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
IFS=$'\n\t'

###############################################################################
# Environment - Feel free to tweak these
###############################################################################

# $_ME
#
# Set to the program's basename.
_ME=$(basename "${0}")

# $_VERSION
#
# Manually set this to to current version of the program. Adhere to the
# semantic versioning specification: http://semver.org
_VERSION="0.0.1"

# dotfile git repo url
_DOTFILE_REPO="https://gitlab.com/Etzeitet/dotfiles.git"


###############################################################################
# Colours!
##############################################################################
_COL_NC='\e[0m' # No Color
_COL_LIGHT_GREEN='\e[1;32m'
_COL_LIGHT_RED='\e[1;31m'
_COL_CYAN='\e[1;36m'
_COL_LIGHT_BLUE='\e[1;94m'
_COL_LIGHT_YELLOW='\e[1;93m'
_TICK="[${_COL_LIGHT_GREEN}✓${_COL_NC}]"
_CROSS="[${_COL_LIGHT_RED}✗${_COL_NC}]"
_INFO="[${_COL_CYAN}i${_COL_NC}]"
# shellcheck disable=SC2034
_DONE="${_COL_LIGHT_GREEN} done!${_COL_NC}"
_OVER="\\r\\033[K"
_DEBUG="[${_COL_LIGHT_BLUE}→${_COL_NC}]"
_YAY="[${_COL_LIGHT_YELLOW}★${_COL_NC}]"

###############################################################################
# Pretty Print
###############################################################################
_DOING=0
_doing() {
    _DOING=1
    printf " %b %s" "${_INFO}" "${1}"
}

_done() {
    _DOING=0
    printf " %b %b %s\\n" "${_OVER}" "${_TICK}" "${1}"
}

_done_error() {
    _DOING=0
    printf " %b %b %s\\n" "${_OVER}" "${_CROSS}" "${1}"
}

_info() {
    printf " %b %s\\n" "${_INFO}" "${1}"
}

_yay() {
    local decoration="${_COL_LIGHT_YELLOW}★ ★${_COL_NC}"
    printf "\\n %b %s %b\\n" "${decoration}" "${1}" "${decoration}" 
}

_print_list() {
    local output
    local default_title
    default_title="Items:"
    
    output="$(printf "%s\\n" "${2:-default_title}"; ret=$?; echo .; exit "${ret}")"
    output="${output%.}"
    
    while read -r line; do
        output+="$(printf "     %s\\n" "${line}"; ret=$?; echo .; exit "${ret}")"
        output="${output%.}"
    done <<< "${1}"

    printf "%s\\n" "${output}"
}

_info_list() {
    _info "$(_print_list "${1}" "${2}")"
}

_debug_list() {
    if [[ ${_USE_DEBUG:-"0"} -eq 1 ]]; then
        _debug echo "$(_print_list "${1}" "${2}")"
    echo ""
    fi
}
###############################################################################
# Debug
###############################################################################

# _debug()
#
# Usage:
#   _debug printf "Debug info. Variable: %s\n" "$0"
#
# A simple function for executing a specified command if the `$_USE_DEBUG`
# variable has been set. The command is expected to print a message and
# should typically be either `echo`, `printf`, or `cat`.
_debug() {
    if [[ "${_USE_DEBUG:-"0"}" -eq 1 ]]; then
        local str
        str=$("${@}")
        
        if [[ ${_DOING} -eq 1 ]]; then
            printf "\\n"
            _DOING=0
        fi

        printf " %b ${_COL_LIGHT_BLUE}%s${_COL_NC}\\n" "${_DEBUG}" "${str}"
    fi
}
# debug()
#
# Usage:
#   debug "Debug info. Variable: $0"
#
# Print the specified message if the `$_USE_DEBUG` variable has been set.
#
# This is a shortcut for the _debug() function that simply echos the message.
debug() {
    echo "${@}"
}

###############################################################################
# Die
###############################################################################

# _die()
#
# Usage:
#   _die printf "Error message. Variable: %s\n" "$0"
#
# A simple function for exiting with an error after executing the specified
# command. The command is expected to print a message and should typically
# be either `echo`, `printf`, or `cat`.
_die() {
  # Prefix die message with "cross mark (U+274C)", often displayed as a red x.
  printf "\\n %b" "${_CROSS} "
  "${@}" 1>&2
  exit 1
}
# die()
#
# Usage:
#   die "Error message. Variable: $0"
#
# Exit with an error and print the specified message.
#
# This is a shortcut for the _die() function that simply echos the message.
die() {
  _die echo "${@}"
}


###############################################################################
# Create Dir
###############################################################################

_create_dir() {
  local _DIR="${1:-}"
  local _WITH_PARENTS="${2:-false}"

  if [[ -z "${_DIR}" ]]; then
    _die printf "No directory name specified.\\n"
  fi

  if [[ -f "${_DIR}" ]]; then
    _die printf "Path %s already exists and is a file\\n" "${_DIR}"
  fi

  if [[ -d "${_DIR}" ]]; then
    _debug printf "%s already exists, nothing to do.\\n" "${_DIR}"
    return 0
  fi

  if [[ "true" == "${_WITH_PARENTS}" ]]; then
    _debug printf "Creating directory: mkdir -p %s\\n" "${_DIR}"
    mkdir -p "${_DIR}"
  else
    _debug printf "Creating directory: mkdir %s\\n" "${_DIR}"
    mkdir "${_DIR}"
  fi
}

###############################################################################
# Help
###############################################################################

# _print_help()
#
# Usage:
#   _print_help
#
# Print the program help information.
_print_help() {
  cat <<HEREDOC
${_ME} - ver. ${_VERSION}

This script automates the deployment of these dotfiles to a Linux or MacOS system.

A bare git repo will be created in $HOME/.dotfiles (which can be overridden with
--dotfilepath) which allows you to manage your changes with git in-place. If you
don't want this behaviour, you can disable it with --donotgitmanage.

If you choose to let git manage the files in your $HOME directory, it is
recommended you clone/fork this repo into your own github/gitlab/gitwhatever
repo. Using the --newupstream option will let you specify a new upstream repo
so you can push future changes. This option is not needed if you modified this
script to use a different repo to start with.

It is an exercise left to the user on how to set up a new repo.

Usage:
  ${_ME} [--donotgitmanage] [--dotfilegitpath /new/path] [--dotfileinstallpath /new/path] [--newupstream https://repo-url/repo.git] [--debug]
  ${_ME} -h | --help

Options:
  -h --help        		Display this help information.

  --donotgitmanage		By default, dot files will be installed using git and
						the repo will be set up as a bare repo. 
                        
                        This option will use git to grab the dot files but not 
                        set up a local bare repo in $HOME. It will instead 
                        clone the repo to --dotfilegitpath (or $HOME/.dotfiles by 
                        default). This lets the user manually move dotfiles as 
                        they see fit. 
                        
                        This is recommended for existing systems or if you
                        want to cherry pick which files you want.


  --dotfilegitpath		Set the preferred path for the bare git repository tree
                        directory (where git lives). The default is 
                        $HOME/.dotfiles. If --donotgitmanage is set, a normal
                        repo will be created in this directory.

  --dotfileinstallpath  Set the preferred installation path for the dotfiles.
                        The default is $HOME, but you might have a weird setup!

  --newupstream         Configures the freshly cloned repo to use a new remote
                        repo.

  --backuppath          Defaults to $HOME/dotfile_backup.

  --debug  		        Prints debugging information.

HEREDOC
}

_print_version() {
  cat <<HEREDOC
  ${_ME} - version ${_VERSION}
HEREDOC
}

###############################################################################
# Options
#
# NOTE: The `getops` builtin command only parses short options and BSD `getopt`
# does not support long arguments (GNU `getopt` does), so the most portable
# and clear way to parse options is often to just use a `while` loop.
#
# For a pure bash `getopt` function, try pure-getopt:
#   https://github.com/agriffis/pure-getopt
#
# More info:
#   http://wiki.bash-hackers.org/scripting/posparams
#   http://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html
#   http://stackoverflow.com/a/14203146
#   http://stackoverflow.com/a/7948533
#   https://stackoverflow.com/a/12026302
#   https://stackoverflow.com/a/402410
###############################################################################

# Parse Options ###############################################################

# Initialize program option variables.
_PRINT_HELP=0
_USE_DEBUG=0
#_BE_QUIET=0
_PRINT_VERSION=0

# Initialize additional expected option variables.
# Defaults could be set here if required
_DO_NOT_GIT_MANAGE=0
_DOT_FILE_INSTALL_PATH="$HOME"
_DOT_FILE_GIT_PATH="$HOME/.dotfiles"
_NEW_UPSTREAM=""
_BACKUP_DIR_PATH="$HOME/dotfile_backup"

# _require_argument()
#
# Usage:
#   _require_argument <option> <argument>
#
# If <argument> is blank or another option, print an error message and  exit
# with status 1.
_require_argument() {
  # Set local variables from arguments.
  #
  # NOTE: 'local' is a non-POSIX bash feature and keeps the variable local to
  # the block of code, as defined by curly braces. It's easiest to just think
  # of them as local to a function.
  local _option="${1:-}"
  local _argument="${2:-}"

  if [[ -z "${_argument}" ]] || [[ "${_argument}" =~ ^- ]]
  then
    _die printf "Option requires a argument: %s\\n" "${_option}"
  fi
}

while [[ ${#} -gt 0 ]]
do
  __option="${1:-}"
  __maybe_param="${2:-}"
  case "${__option}" in
    -h|--help)
      _PRINT_HELP=1
      ;;
    --debug)
      _USE_DEBUG=1
      ;;
    --version)
      _PRINT_VERSION=1
      ;;
    --donotgitmanage)
      _DO_NOT_GIT_MANAGE=1
      ;;
    --dotfilegitpath)
	  _require_argument "${__option}" "${__maybe_param}"
      _DOT_FILE_GIT_PATH="${__maybe_param}"
      ;;
    --dotfileinstallpath)
      _require_argument "${__option}" "${__maybe_param}"
      _DOT_FILE_INSTALL_PATH="${__maybe_param}"
      ;;
    --newupstream)
      _require_argument "${__option}" "${__maybe_param}"
      _NEW_UPSTREAM="${__maybe_param}"
	  ;;
    --backuppath)
      _require_argument "${__option}" "${__maybe_param}"
      _BACKUP_DIR_PATH="${__maybe_param}"
	  ;;
    --endopts)
      # Terminate option parsing.
      break
      ;;
    -*)
      _die printf "Unexpected option: %s\\n" "${__option}"
      ;;
  esac
  shift
done


###############################################################################
# Internal Variables
#
# You should probably avoid changing these... but hey, you do you!
###############################################################################

# This gets used a fair bit, so better to define it here rather than
# typing it over and over...
_GIT_CMD_PREFIX=(
    'git' 
    '--git-dir'
    "${_DOT_FILE_GIT_PATH}"
    '--work-tree'
    "${_DOT_FILE_INSTALL_PATH}"
)


###############################################################################
# Program Functions
###############################################################################

###############################################################################
# Check all arguments, catching any required and crying about them
###############################################################################

_check_args() {

  if [[ -n "${_VERSION}" ]]; then
      _debug printf "version: %s" "${_VERSION}"
  fi

  if [[ -n "${_USE_DEBUG}" ]]; then
      _debug printf "debug: %s" "${_USE_DEBUG}"
  fi

  if [[ -n "${_DO_NOT_GIT_MANAGE}" ]]; then
      _debug printf "_DO_NOT_GIT_MANAGE: %s" "${_DO_NOT_GIT_MANAGE}"
  fi

  if [[ -n "${_DOT_FILE_GIT_PATH}" ]]; then
      _debug printf "_DOT_FILE_GIT_PATH: %s" "${_DOT_FILE_GIT_PATH}"
  fi
  
  if [[ -n "${_DOT_FILE_INSTALL_PATH}" ]]; then
      _debug printf "_DOT_FILE_INSTALL_PATH: %s" "${_DOT_FILE_INSTALL_PATH}"
  fi
  
  if [[ -n "${_NEW_UPSTREAM}" ]]; then
      _debug printf "_NEW_UPSTREAM: %s" "${_NEW_UPSTREAM}"
  fi
  
  if [[ -n "${_BACKUP_DIR_PATH}" ]]; then
      _debug printf "_BACKUP_DIR_PATH: %s" "${_BACKUP_DIR_PATH}"
  fi
}


###############################################################################
# Main
###############################################################################

# _main()
#
# Usage:
#   _main [<options>] [<arguments>]
#
# Description:
#   Entry point for the program, handling basic option parsing and dispatching.
_main() {
  if ((_PRINT_HELP)); then
    _print_help
  elif ((_PRINT_VERSION)); then
    _print_version
  else
    _check_args "$@"
    _check_git
    _clone_repo
    _backup
    _checkout
    
    if [[ ! -z ${_NEW_UPSTREAM} ]]; then
        _update_upstream
    fi

    _configure_git_repo
    _print_completion_message
  fi
}

_check_git() {
    local str
    str="Checking git is installed"

    #printf " %b %s" "${_INFO}" "${str}"
    _doing "${str}"

    if gitpath=$(which git 2>& 1); then

        #printf " %b %b %s\\n" "${_OVER}" "${_TICK}" "${str}"
        _done "${str}"
        
        _debug printf "git binary found at %s\\n" "${gitpath}"
    
    else
        _die printf "git binary not found on PATH\\n"
    fi
}

_clone_repo() {
    local str
    local git_cmd_start
    local git_cmd_bare
    local git_cmd_end

    str="Cloning ${_DOTFILE_REPO} into ${_DOT_FILE_GIT_PATH}"
    printf " %b %s" "${_INFO}" "${str}"
   
    git_cmd_start=('git' 'clone' '--depth' '1') 
    git_cmd_bare=('--bare')
    git_cmd_end=("${_DOTFILE_REPO}" "${_DOT_FILE_GIT_PATH}" "2>& 1")
    cmd=(${git_cmd_start[@]})

    if [[ ${_DO_NOT_GIT_MANAGE} -eq 0 ]]; then
        cmd+=(${git_cmd_bare[@]})
    fi
        
    cmd+=(${git_cmd_end[@]})
    if output=$(eval "${cmd[@]}"); then
        
        _done "${str}"
        _debug printf "${output}\\n"
    
    else

        _done_error "${str} - ${output}"
        _die printf "Exiting: git clone failed!\\n"
    
    fi
}

_backup() {
    local srcpath
    local destpath
    local dotfile_list
    local file_count
    local str
    
    str="Checking for existing dotfiles to backup"
    _doing "${str}"

    dotfile_list="$("${_GIT_CMD_PREFIX[@]}" ls-tree -r master --name-only)"
    
    _debug_list "${dotfile_list}" "Checking for these files:"
    
    file_count=0
    while read -r file; do

        srcpath="${_DOT_FILE_INSTALL_PATH}/${file}"
        destpath="${_BACKUP_DIR_PATH}/${file}"
        destdir=$(dirname "${destpath}")

        if [[ -f ${srcpath} ]]; then

            if [[ ! -d ${destdir} ]]; then
                _create_dir "${destdir}" true
            fi

            if mv "${srcpath}" "${destpath}"; then

                file_count=$((file_count + 1))
                _debug printf "Backed up %s to %s\\n" "${srcpath}" "${destpath}"
            
            else

                _done_error "${str} - Failed"
                _die printf "File %s backup failed. Aborting script" "${srcpath}"
            fi
        else
            _debug printf "%s not found, skipping.\\n" "${srcpath}"
        fi
    done <<< "${dotfile_list}"

    if [[ ${file_count} -eq 0 ]]; then
        alpha="No files"
    elif [[ ${file_count} -eq 1 ]]; then
        alpha="1 file"
    else
        alpha="${file_count} files"
    fi

    _done "${str}"
    _info "$(printf "%s backed up to %s" "${alpha}" "${_BACKUP_DIR_PATH}")"
}

_checkout() {
    local str
    local cmd
    local output

    str="Checking out files to ${_DOT_FILE_INSTALL_PATH}"
   
    _doing "${str}"
    
    cmd=( "${_GIT_CMD_PREFIX[@]}" checkout )
    if output=$(eval "${cmd[@]}"); then
        _done "${str}"
    else
        _done_error "${str} - ${output}"
        _die "Checkout failed!"
    fi
}

_configure_git_repo() {
    local str
    local basename

    str="Configuring the repo to ignore untracked files"
    _doing "${str}"

    cmd=( "${_GIT_CMD_PREFIX[@]}" config status.showUntrackedFiles no )
    if output=$(eval "${cmd[@]}"); then
        _done "${str}"
    else
        _done_error "${str} - ${output}"
        _die "Could not configure git"
    fi


    str="Adding ${_DOT_FILE_GIT_PATH} to .gitignore"
    _doing "${str}"

    basename="$(basename "${_DOT_FILE_GIT_PATH}")"

    # shellcheck disable=SC2116
    if output="$(echo "${basename}" >> .gitignore)"; then
        _done "${str}"
    else
        _done_error "${str} - ${output}"
        _die "Could not configure git"
    fi
}

_update_upstream() {
    local cmd
    local str

    str="Updating repo origin to ${_NEW_UPSTREAM}"
    _doing "${str}"

    cmd=('git' '--git-dir' "${_DOT_FILE_GIT_PATH}" remote set-url origin "${_NEW_UPSTREAM}")
    if output=$(eval "${cmd[@]}"); then
        _done "$str"
    else
        _done_error "${str} - ${output}"
    fi
}

_print_completion_message() {
    local worktree
    local gitdir
    
    # shellcheck disable=SC2016
    worktree="${_DOT_FILE_INSTALL_PATH/$HOME/'$HOME'}"
    # shellcheck disable=SC2016
    gitdir="${_DOT_FILE_GIT_PATH/$HOME/'$HOME'}"

    _yay "Installation of your dotfiles is complete!"

    cat <<HEREDOC
     
     As you chose to let git manage your dot files in-place, you need to use a 
     modified git command to interact with the repo, for example:

     git --git-dir=${gitdir} --work-tree=${worktree} status

     To make this simpler, you could add an alias to your shell's config file:

     alias dotconf='git --git-dir=${gitdir} --work-tree=${worktree}'

     e.g.,

     dotconf status
     dotconf add
     etc

     The .zshrc config already has this alias, but you may need to update it
     if you installed outside of the defaults.
HEREDOC
}

# Call `_main` after everything has been defined.
_main "$@"
