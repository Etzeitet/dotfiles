source ~/.vim/plugins.vim

syntax on
color dracula

filetype plugin indent on

set autoread

let mapleader = ","

nmap <leader>w :w!<cr>

command W w !sudo tee % > /dev/null

" list buffers
nnoremap gb :ls<CR>:b<Space>

"run current buffer through python
nnoremap <leader>p :w !python<cr>

"run current visual selection through python
xnoremap <leader>p :w !python<cr>

set ruler
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
set ignorecase
set smartcase
set hlsearch
set incsearch
set lazyredraw
set magic
set showmatch
set noerrorbells
set novisualbell
set t_vb=
set tm=500
set nobackup
set nowb
set noswapfile
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set lbr
set tw=500
set ai
set si
set wrap
set number
"set numberwidth=5


if !has('gui_running')
  set t_Co=256
endif


" Lightline Plugin
set laststatus=2
set noshowmode

let g:lightline = {
  \     'active': {
  \         'left': [['mode', 'paste' ], ['readonly', 'filename', 'modified']],
  \         'right': [['lineinfo'], ['percent'], ['fileformat', 'fileencoding']]
  \     }
  \ }


" FZF Plugin
map ; :Files<CR>

